<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Products</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content_box mb_40">
                        <div class="content_box__heading">
                            <h2>Landing Page</h2>
                        </div>

                        <div class="card">
                            <div class="card__left">
                                <div class="card__photo">
                                    <img src="images/no_photo.jpg" class="img-fluid" about="">
                                </div>
                                <label class="btn btn_yellow btn_long">
                                    <span>ADD PHOTO</span>
                                    <input type="file" name="" class="">
                                </label>
                            </div>
                            <div class="card__center">
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Facebook</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="https://www.facebook.com/vexaglobal">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Instagram</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="https://www.instagram.com/vegaglobal">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">WhatsApp</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="+1(001)123-45-67">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">WeChat</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="id or +86(001)123-45-67">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Who am I?</label>
                                            <textarea class="form_control" name="m1" placeholder="" rows="6">Hello, my name is Tom, I'm from the USA.  I am teaching young professionals in technologies to work with blockchain and cryptocurrencies in general.  I highly recommend Vexa Global because it is a completely legal international company with a funded portfolio containing investments in cryptocurrency startups such as ATM, exchange, trading, etc.</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card__right">
                                <div class="form_group">
                                    <label class="form_label">Telegram</label>
                                    <input type="text" class="form_control" name="n1" placeholder="" value="@id or +1(001)123-45-67">
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Short info about me</label>
                                    <input type="text" class="form_control" name="n1" placeholder="" value="Trader, investor, coach, financial analytic">
                                </div>
                                <div class="form_group">
                                    <button type="button" class="btn btn_yellow btn_long btn_save"><span>SAVE</span></button>
                                </div>
                                <div class="card__link">
                                    <label class="form_label">Your website</label>
                                    <input type="text" class="form_control form_control__link" name="n1" placeholder="" value="Trader, investor, coach, financial analytic">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col col-xs-12 col-lg-6 col-gutter-lr mb_40">
                            <div class="content_box content_box_height">
                                <div class="content_box__heading">
                                    <h3>Statements</h3>
                                </div>
                                <div class="content_box__table">
                                    <table class="table_main">
                                        <thead>
                                        <tr>
                                            <th class="text-uppercase  td_glow">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_date.png" alt=""></i>
                                                    <span>PERIOD</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_money.png" alt=""></i>
                                                    <span>PACKAGES</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_cloud.png" alt=""></i>
                                                    <span>AFF. INC.</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_cloud.png" alt=""></i>
                                                    <span>DOWNLOAD</span>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td data-header="PERIOD" class="text-nowrap"><span>2019-08-23  18:56:29</span></td>
                                            <td data-header="PACKAGES"><span>something</span></td>
                                            <td data-header="AFF. INC." class="text-nowrap"><span>$12,000</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        <tr>
                                            <td data-header="PERIOD" class="text-nowrap"><span>2019-08-23  18:56:29</span></td>
                                            <td data-header="PACKAGES"><span>something</span></td>
                                            <td data-header="AFF. INC." class="text-nowrap"><span>$12,000</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        <tr>
                                            <td data-header="PERIOD" class="text-nowrap"><span>2019-08-23  18:56:29</span></td>
                                            <td data-header="PACKAGES"><span>something</span></td>
                                            <td data-header="AFF. INC." class="text-nowrap"><span>$12,000</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-lg-6 col-gutter-lr mb_40">
                            <div class="content_box content_box_height">
                                <div class="content_box__heading">
                                    <h3>Trading strategies</h3>
                                </div>
                                <div class="content_box__table">
                                    <table class="table_main">
                                        <thead>
                                        <tr>
                                            <th class="text-uppercase td_glow">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_date.png" alt=""></i>
                                                    <span>NAME</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_cloud.png" alt=""></i>
                                                    <span>DOWNLOAD</span>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td data-header="PERIOD"><span>What is Moving Average Convergence Divergence (MACD)	</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        <tr>
                                            <td data-header="PERIOD"><span>What is relative strength index	</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        <tr>
                                            <td data-header="PERIOD"><span>What is relative strength index	</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
