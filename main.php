<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Dashboard</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="message message_yellow mb_30">o confirm and finish withdrawal process - please check <span class="btn btn_yellow btn_sm">your email</span> (also SPAM folder). If you use google authenticator, just ignore this message</div>

                    <div class="dashboard">
                        <div class="dashboard__four">
                            <div class="informer">
                                <div class="informer__large">
                                    <div class="informer__data">
                                        <span>MAIN BALANCE</span>
                                        <strong>$ 259.50</strong>
                                    </div>
                                    <div class="dashboard_icon">
                                        <img src="img/dashboard__icon_01.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard__four">
                            <div class="informer">
                                <div class="informer__large">
                                    <div class="informer__data">
                                        <span>FUND BALANCE</span>
                                        <strong>$ 10.00</strong>
                                    </div>
                                    <div class="dashboard_icon">
                                        <img src="img/dashboard__icon_02.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard__four">
                            <div class="informer">
                                <div class="informer__large">
                                    <div class="informer__data">
                                        <span>AFFILIATE BALANCE</span>
                                        <strong>$ 330.75</strong>
                                    </div>
                                    <div class="dashboard_icon">
                                        <img src="img/dashboard__icon_03.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard__four">
                            <div class="informer">
                                <div class="informer__mini">
                                    <div class="informer__mini_elem informer__mini_01">
                                        <span>BASIC</span>
                                        <strong>$ 120.00</strong>
                                    </div>
                                    <div class="informer__mini_elem informer__mini_02">
                                        <span>EXCLUSIVE</span>
                                        <strong>$ 0.75</strong>
                                    </div>
                                    <div class="informer__mini_elem informer__mini_03">
                                        <span>PREMIUM</span>
                                        <strong>$ 60.00</strong>
                                    </div>
                                    <div class="informer__mini_elem informer__mini_04">
                                        <span>POINTS</span>
                                        <strong>$ 1,250.75</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard">

                        <div class="dashboard__three">
                            <div class="informer">
                                <div class="informer__notice">
                                    <div class="informer__notice_heading">Notifications</div>
                                    <div class="informer__notice_scroll">
                                        <ul class="notice_line">
                                            <li>
                                                <div class="notice_line__title">System update 2.0</div>
                                                <div class="notice_line__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies</div>
                                                <div class="notice_line__date">22 June 2019</div>
                                            </li>
                                            <li>
                                                <div class="notice_line__title">New Level 5</div>
                                                <div class="notice_line__text">Our passion are cryptocurrencies and their role in changing world.</div>
                                                <div class="notice_line__date">16 June 2019</div>
                                            </li>
                                            <li>
                                                <div class="notice_line__title">System update 2.0</div>
                                                <div class="notice_line__text">We are a team of technology and finance hotheads. Our passion are cryptocurrencies</div>
                                                <div class="notice_line__date">22 June 2019</div>
                                            </li>
                                            <li>
                                                <div class="notice_line__title">New Level 5</div>
                                                <div class="notice_line__text">Our passion are cryptocurrencies and their role in changing world.</div>
                                                <div class="notice_line__date">16 June 2019</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard__three">
                            <div class="informer">
                                <ul class="informer__index">
                                    <li>
                                        <div class="dashboard_icon">
                                            <img src="img/dashboard__icon_04.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_text">
                                            <strong>$ 310.00</strong>
                                            <span>MY PACKAGES</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dashboard_icon">
                                            <img src="img/dashboard__icon_05.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_text">
                                            <strong>$ 1,528.00</strong>
                                            <span>Affiliate income</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dashboard_icon">
                                            <img src="img/dashboard__icon_06.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="informer__index_text">
                                            <strong>$ 3,456.50</strong>
                                            <span>total Sales</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="dashboard__three dashboard__three_last">
                            <div class="informer">
                                <div class="informer__stage">

                                    <div class="informer__stage_level">
                                        <div class="informer__level">
                                            <strong>5</strong>
                                            <span>level</span>
                                        </div>
                                        <ul class="informer__stage_data">
                                            <li>Ref. percent: <strong>6.00%</strong></li>
                                            <li>Next bonus: <strong>$ 200</strong></li>
                                            <li>Need turnover: <strong>$ 10,000</strong></li>
                                        </ul>
                                    </div>

                                    <div class="informer__stage_charts">

                                        <div class="informer__stage_row">

                                            <div class="informer__stage_col">
                                                <div class="informer__stage_doughnut">
                                                    <canvas id="myChart1" width="80" height="80"></canvas>
                                                    <div class="informer__stage_value">
                                                        <div class="informer__stage_value_wrap">
                                                            <strong>1st</strong>
                                                            <span>LEG</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="informer__stage_legend">
                                                    <div><strong>167</strong> / <span>5000</span></div>
                                                    <div>Sales</div>
                                                </div>
                                            </div>

                                            <div class="informer__stage_col">
                                                <div class="informer__stage_doughnut">
                                                    <canvas id="myChart2" width="80" height="80"></canvas>
                                                    <div class="informer__stage_value">
                                                        <div class="informer__stage_value_wrap">
                                                            <strong>2st</strong>
                                                            <span>LEG</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="informer__stage_legend">
                                                    <div><strong>167</strong> / <span>5000</span></div>
                                                    <div>Sales</div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="dashboard">
                        <div class="dashboard__two">
                            <div class="informer">
                                <div class="informer_chart">
                                    <div class="informer_chart__legend">
                                        <div class="informer_chart__legend_title">Partners</div>
                                        <ul class="informer_chart__legend_values">
                                            <li><strong>88</strong> <span>Total partners</span></li>
                                            <li><strong>36</strong> <span>Active partners</span></li>
                                        </ul>
                                    </div>
                                    <div class="informer_chart__content">
                                        <canvas id="myLine"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard__two">
                            <div class="informer">
                                <div class="informer_chart">
                                    <div class="informer_chart__legend">
                                        <div class="informer_chart__legend_title">Weekly turnover</div>
                                        <ul class="informer_chart__legend_values">
                                            <li class="no_point"><strong>$345.00</strong> <span>Total turnover</span></li>
                                        </ul>
                                    </div>
                                    <div class="informer_chart__content">
                                        <div class="chart">
                                            <ul class="bars" id="bars">
                                                <li><div data-percentage="56" class="bar" style="height: 56%;"><div class="bars_tooltip">$352</div></div><span>28.04.19</span></li>
                                                <li><div data-percentage="33" class="bar" style="height: 33%;"><div class="bars_tooltip">$352</div></div><span>29.04.19</span></li>
                                                <li><div data-percentage="54" class="bar" style="height: 54%;"><div class="bars_tooltip">$352</div></div><span>30.04.19</span></li>
                                                <li><div data-percentage="91" class="bar" style="height: 91%;"><div class="bars_tooltip">$352</div></div><span>1.05.19</span></li>
                                                <li><div data-percentage="44" class="bar" style="height: 44%;"><div class="bars_tooltip">$352</div></div><span>2.05.19</span></li>
                                                <li><div data-percentage="23" class="bar" style="height: 23%;"><div class="bars_tooltip">$352</div></div><span>3.05.19</span></li>
                                                <li><div data-percentage="55" class="bar" style="height: 55%;"><div class="bars_tooltip">$352</div></div><span>4.05.19</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard">

                        <div class="dashboard__two">
                            <div class="box">
                                <div class="box__heading">Latest Registrations</div>
                                <div class="box__content">
                                    <div class="table">
                                        <div class="table__header">
                                            <table class="table_header">
                                                <tr>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__01.png" alt=""></i>
                                                            <span>Partner ID</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__02.png" alt=""></i>
                                                            <span>Upline</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__03.png" alt=""></i>
                                                            <span>Inviter</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__04.png" alt=""></i>
                                                            <span>Date</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="table__content_scroll">
                                            <div class="table__content">
                                                <table class="table_content">
                                                    <tr>
                                                        <td data-header="Partner ID"><span>Nazar</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>olgamuzikant</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>ladyrich88</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>Mady</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>jeine25</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>seba</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>

                                                    <tr>
                                                        <td data-header="Partner ID"><span>Nazar</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>olgamuzikant</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>ladyrich88</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>Mady</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>jeine25</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Partner ID"><span>seba</span></td>
                                                        <td data-header="Upline"><span>VX271962</span></td>
                                                        <td data-header="Inviter"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard__two">
                            <div class="box">
                                <div class="box__heading">Latest Referal Yield</div>
                                <div class="box__content">
                                    <div class="table">
                                        <div class="table__header">
                                            <table class="table_header">
                                                <tr>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__01.png" alt=""></i>
                                                            <span>Ref. Income</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__02.png" alt=""></i>
                                                            <span>Invest</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__03.png" alt=""></i>
                                                            <span>Partner</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__04.png" alt=""></i>
                                                            <span>Date</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="table__content_scroll">
                                            <div class="table__content">
                                                <table class="table_content">
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$14.27</span></td>
                                                        <td data-header="Invest"><span>$142.70</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$4.14</span></td>
                                                        <td data-header="Invest"><span>$41.40</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$7.05</span></td>
                                                        <td data-header="Invest"><span>$70.50</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$5.30</span></td>
                                                        <td data-header="Invest"><span>$53.00</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$5.00</span></td>
                                                        <td data-header="Invest"><span>$50.00</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td data-header="Ref. Income"><span>$117.75</span></td>
                                                        <td data-header="Invest"><span>$1177.50</span></td>
                                                        <td data-header="Partner"><span>VX271962</span></td>
                                                        <td data-header="Date"><span>22 june 2019</span></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="dashboard">

                        <div class="dashboard__two">
                            <div class="box">
                                <div class="box__heading">Latest Registrations</div>
                                <div class="box__content">
                                    <div class="table">
                                        <div class="table__header">
                                            <table class="table_header">
                                                <tr>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__01.png" alt=""></i>
                                                            <span>Partner ID</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__02.png" alt=""></i>
                                                            <span>Upline</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__03.png" alt=""></i>
                                                            <span>Inviter</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_yellow__04.png" alt=""></i>
                                                            <span>Date</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="table__content_scroll">
                                            <div class="table__content">
                                                <div class="box__empty">
                                                    <div class="box__empty_wrap">Nothing to display</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dashboard__two">
                            <div class="box">
                                <div class="box__heading">Latest Referal Yield</div>
                                <div class="box__content">
                                    <div class="table">
                                        <div class="table__header">
                                            <table class="table_header">
                                                <tr>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__01.png" alt=""></i>
                                                            <span>Ref. Income</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__02.png" alt=""></i>
                                                            <span>Invest</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__03.png" alt=""></i>
                                                            <span>Partner</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table_header__wrap">
                                                            <i><img src="img/table_icon_blue__04.png" alt=""></i>
                                                            <span>Date</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="table__content_scroll">
                                            <div class="table__content">
                                                <div class="box__empty">
                                                    <div class="box__empty_wrap">Nothing to display</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>


        <?php include('inc/scripts.inc.php') ?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
        <script>

            // Chart 1
            var ctx1 = document.getElementById('myChart1').getContext('2d');
            var myChart1 = new Chart(ctx1, {
                type: 'doughnut',
                data: {
                    labels: ['Red', 'Blue'],
                    datasets: [{
                        label: '# of Votes',
                        data: [167, 5000],
                        backgroundColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(34, 60, 94, 0.2)'
                        ],
                        borderColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(34, 60, 94, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 95,
                    legend: {
                        display: false
                    },
                    scales: {
                        display: false
                    }
                }
            });

            // Chart 2
            var ctx2 = document.getElementById('myChart2').getContext('2d');
            var myChart2 = new Chart(ctx2, {
                type: 'doughnut',
                data: {
                    labels: ['Red', 'Blue'],
                    datasets: [{
                        label: '# of Votes',
                        data: [2765, 5000],
                        backgroundColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(34, 60, 94, 0.2)'
                        ],
                        borderColor: [
                            'rgba(0, 145, 255, 1)',
                            'rgba(34, 60, 94, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 95,
                    legend: {
                        display: false
                    },
                    scales: {
                        display: false
                    }
                }
            });


            // Chart 3
            var ctx3 = document.getElementById('myLine').getContext('2d');

            var gradientLine = ctx3.createLinearGradient(0, 0, 0, 300);
            gradientLine.addColorStop(0, 'rgba(0,143,254,1)');
            gradientLine.addColorStop(1, 'rgba(28,52,93,0)');

            var myLineChart = new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: ['28.04.19', '29.04.19', '30.04.19', '1.05.19', '2.05.19', '3.05.19', '4.05.19'],
                    datasets: [{
                        label: '',
                        data: [15, 30, 25, 20, 14, 28, 38],
                        borderColor: [
                            'rgba(0, 145, 255, 1)'
                        ],
                        backgroundColor: gradientLine,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });


        </script>

    </body>
</html>
