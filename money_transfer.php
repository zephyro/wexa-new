<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Money transfer</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                            <div class="content_box content_box_height  mb_40">
                                <div class="content_box__heading">
                                    <h3>Transfer between your wallets</h3>
                                </div>
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Send from</label>
                                            <select class="form_control form_select" name="s1">
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <select class="form_control form_select" name="s2">
                                                <option value="Fund balance">Fund balance</option>
                                                <option value="Fund balance">Fund balance</option>
                                                <option value="Fund balance">Fund balance</option>
                                                <option value="Fund balance">Fund balance</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Amount</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="10,000">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <button type="button" class="btn btn_yellow btn_long btn_exchange"><span>EXCHANGE</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr">
                            <div class="content_box content_box_height  mb_40">
                                <div class="content_box__heading">
                                    <h3>Transfer between accounts</h3>
                                </div>
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Select balance</label>
                                            <select class="form_control form_select" name="s1">
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                                <option value="Main balance">Main balance</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Enter email or ID of recipient</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="v@gmail.com">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Amount</label>
                                            <input type="text" class="form_control" name="n1" placeholder="" value="15,000">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <button type="button" class="btn btn_yellow btn_long btn_exchange"><span>EXCHANGE</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
