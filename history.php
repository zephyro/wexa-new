<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content_box">
                        <div class="content_filter">
                           <ul class="content_filter__left">
                               <li>
                                   <div class="filter">
                                       <div class="filter__legend">Type of transaction</div>
                                       <div class="filter__content">

                                           <div class="filter_list">
                                               <div class="filter_list__label">
                                                   <div class="filter_list__value" data-value="All transactions">All transactions</div>
                                               </div>
                                               <div class="filter_list__dropdown">
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f1" value="0">
                                                       <span>All transactions</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f2" value="Refill">
                                                       <span>Refill</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f3" value="Money transfer">
                                                       <span>Money transfer</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f4" value="Payout">
                                                       <span>Payout</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f5" value="Exchange">
                                                       <span>Exchange</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Ref. income">
                                                       <span>Ref. income</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="New level">
                                                       <span>New level</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Packages">
                                                       <span>Packages</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Bonus">
                                                       <span>Bonus</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Partnership act.">
                                                       <span>Partnership act.</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Balance transfer inv.">
                                                       <span>Balance transfer inv.</span>
                                                   </label>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </li>
                               <li>
                                   <div class="filter">
                                       <div class="filter__legend">Status</div>
                                       <div class="filter__content">

                                           <div class="filter_list">
                                               <div class="filter_list__label">
                                                   <div class="filter_list__value" data-value="All transactions">All transactions</div>
                                               </div>
                                               <div class="filter_list__dropdown">
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f1" value="0">
                                                       <span>All transactions</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f2" value="Refill">
                                                       <span>Refill</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f3" value="Money transfer">
                                                       <span>Money transfer</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f4" value="Payout">
                                                       <span>Payout</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f5" value="Exchange">
                                                       <span>Exchange</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Ref. income">
                                                       <span>Ref. income</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="New level">
                                                       <span>New level</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Packages">
                                                       <span>Packages</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Bonus">
                                                       <span>Bonus</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Partnership act.">
                                                       <span>Partnership act.</span>
                                                   </label>
                                                   <label class="filter_list__checkbox">
                                                       <input type="checkbox" name="f6" value="Balance transfer inv.">
                                                       <span>Balance transfer inv.</span>
                                                   </label>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </li>
                           </ul>
                            <ul class="content_filter__right">
                                <li>
                                    <div class="form_line">
                                        <input class="form_line__input" type="text" name="email" placeholder="Partner ID" value="">
                                        <button type="button" class="btn btn_yellow btn_sm btn_no_shadow btn_search"><span>SEARCH</span></button>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <table class="table_main table_sort">
                            <thead>
                            <tr>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_date.png" alt=""></i>
                                        <span>DATA</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_money.png" alt=""></i>
                                        <span>TYPE OF TRANSACTION</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_balance.png" alt=""></i>
                                        <span>AMOUNT</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_wait.png" alt=""></i>
                                        <span>STATUS</span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-header="DATA"><div class="text-nowrap">2019-08-21 06:23:49</div></td>
                                <td data-header="TYPE OF TRANSACTION"><div>Refill</div></td>
                                <td data-header="AMOUNT"><div class="text-nowrap">$150,00</div></td>
                                <td data-header="STATUS"><div><span class="status status_open">OPENED</span></div></td>
                            </tr>
                            <tr>
                                <td data-header="DATA"><div class="text-nowrap">2019-08-21 06:23:49</div></td>
                                <td data-header="TYPE OF TRANSACTION"><div>Money transfer</div></td>
                                <td data-header="AMOUNT"><div class="text-nowrap">12,00</div></td>
                                <td data-header="STATUS"><div><span class="status status_closed">CLOSED</span></div></td>
                            </tr>
                            <tr>
                                <td data-header="DATA"><div class="text-nowrap">2019-08-21 06:23:49</div></td>
                                <td data-header="TYPE OF TRANSACTION"><div>Payout</div></td>
                                <td data-header="AMOUNT"><div class="text-nowrap">0,00</div></td>
                                <td data-header="STATUS"><div><span class="status status_processing">Processing</span></div></td>
                            </tr>
                            <tr>
                                <td data-header="DATA"><div class="text-nowrap">2019-08-21 06:23:49</div></td>
                                <td data-header="TYPE OF TRANSACTION"><div>Parthnership act.</div></td>
                                <td data-header="AMOUNT"><div class="text-nowrap">0,00</div></td>
                                <td data-header="STATUS"><div><span class="status status_processing">Processing</span></div></td>
                            </tr>
                            <tr>
                                <td data-header="DATA"><div class="text-nowrap">2019-08-21 06:23:49</div></td>
                                <td data-header="TYPE OF TRANSACTION"><div>Ref. income</div></td>
                                <td data-header="AMOUNT"><div class="text-nowrap">50,00</div></td>
                                <td data-header="STATUS"><div><span class="status status_answer">user`s  answer</span></div></td>
                            </tr>

                            </tbody>
                        </table>

                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><span>...</span></li>
                            <li><a href="#">12</a></li>
                        </ul>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
