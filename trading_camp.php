<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Trading camp</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr mb_40">
                            <div class="content_box">
                                <div class="content_box__heading">
                                    <h3 class="color_yellow">The number of tickets is limited. We offer to visit Trading Camp only for 100 partners.</h3>
                                </div>
                                <div class="mb_20">
                                    <img src="images/img_bnr.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="content_box__text">
                                    We are happy to inform you that we have opened the opportunity to buy tickets for our Winter Trading Camp in Thailand from November 29, 2019 to December 2, 2019! The Winter Trading Camp in Thailand will be held in Hua Hin 5* Resort.<br/>
                                    Day 1 - Overview, Rudiments, Principles of Markets vs Theories;<br/>
                                    Day 2 - Developing a Regiment, Reference Framework: Moving Averages;<br/>
                                    Day 3 - Risk of Loss & Expectation of Profit;<br/>
                                    Day 4 - Portfolio & Money Management.<br/>
                                    Additionally - we have prepared for you a lot of attractions and amenities!<br/>
                                    - traditional Thai & International meals every day,<br/>
                                    - daily massage;<br/>
                                    - optional yoga and meditation sessions;<br/>
                                    - Island cruise: Snorkeling & Jet Ski;<br/>
                                    * and much more!<br/>
                                    ** plane tickets are not included in the price.
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr mb_40">
                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Order form</h3>
                                </div>
                                <form class="form">
                                    <div class="row">
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Select balance</label>
                                                <select class="form_control form_select" name="s1">
                                                    <option value="Main balance">Main balance</option>
                                                    <option value="Main balance">Main balance</option>
                                                    <option value="Main balance">Main balance</option>
                                                    <option value="Main balance">Main balance</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Ticket count</label>
                                                <input class="form_control" type="text" name="n1" placeholder="" value="2">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label">Total price, USD</label>
                                                <input class="form_control" type="text" name="n2" placeholder="" value="15,000">
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                            <div class="form_group">
                                                <label class="form_label"></label>
                                                <button type="submit" class="btn btn_yellow btn_long btn_save"><span>ORDER</span></button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <div class="content_box">
                                <div class="content_box__heading">
                                    <h3>Your tickets</h3>
                                </div>
                                <div class="content_box__table">
                                    <table class="table_main">
                                        <thead>
                                        <tr>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_date.png" alt=""></i>
                                                    <span>DATE</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_ticket.png" alt=""></i>
                                                    <span>TICKETS COUNT</span>
                                                </div>
                                            </th>
                                            <th class="text-uppercase">
                                                <div class="table_main__wrap">
                                                    <i><img src="img/icon__table_cloud.png" alt=""></i>
                                                    <span>DOWNLOAD</span>
                                                </div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td data-header="DATE"><span>2019-08-23  18:56:29</span></td>
                                            <td data-header="TICKETS COUNT"><span>14</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        <tr>
                                            <td data-header="DaTE"><span>2019-11-19  14:26:11</span></td>
                                            <td data-header="TICKETS COUNT"><span>38</span></td>
                                            <td data-header="DOWNLOAD"><span><a href="#">Download</a></span></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
