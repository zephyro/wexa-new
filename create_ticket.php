<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>New ticket</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content">
                        <div class="content__header">
                            <h2>Create ticket</h2>
                        </div>
                        <form class="form">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                    <div class="form_group">
                                        <div class="form_label">Ticket category</div>
                                        <select class="form_control form_select" name="s1">
                                            <option value="Payments">Payments</option>
                                            <option value="Payments">Payments</option>
                                            <option value="Payments">Payments</option>
                                            <option value="Payments">Payments</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                    <div class="form_group">
                                        <div class="form_label">Subject</div>
                                        <select class="form_control form_select" name="s2">
                                            <option value="English">English</option>
                                            <option value="Russian">Russian</option>
                                            <option value="Spain">Spain</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-gutter-lr">
                                    <div class="form_group">
                                        <div class="form_label">Message</div>
                                        <textarea class="form_control" name="message" placeholder="Some support jobs are internal positions, while other positions, like receptionists, interact with customers or clients. Either way, “hiring managers want to assess your interrelationship skills|" rows="7"></textarea>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-gutter-lr">
                                    <div class="flex_group">
                                        <div class="form_group">
                                            <div class="file_form">
                                                <label class="file_form__input">
                                                    <input type="file" name="file">
                                                    <span>ATTACH A FILE</span>
                                                </label>
                                                <div class="file_form__text">(Pdf, doc, docx , Up to 10 MB)</div>
                                                <div class="file_form__source">
                                                    <div class="file_form__item">
                                                        <span>Bitcoin-Symbol.png </span>
                                                        <i class="file_form__remove"></i>
                                                    </div>
                                                    <div class="file_form__item">
                                                        <span>company logo.png </span>
                                                        <i class="file_form__remove"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form_group">
                                            <ul class="btn_group">
                                                <li>
                                                    <button type="reset" class="btn btn_lg" title="Cancel"><span>Cancel</span></button>
                                                </li>
                                                <li>
                                                    <button type="reset" class="btn btn_yellow btn_lg" title="CREATE new ticket"><span>CREATE new ticket</span></button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
