<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Marketing</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="tabs">
                        <div class="tabs__nav">
                            <a data-target=".tab1" class="nav_icon_1 active"><span>PRESENTATION</span></a>
                            <a data-target=".tab2" class="nav_icon_2"><span>Banners</span></a>
                            <a data-target=".tab3" class="nav_icon_3"><span>BONUS CAR & REGIONAL OFFICES PROGRAMS</span></a>
                            <a data-target=".tab4" class="nav_icon_4"><span>VEXA GLOBAL BRAND</span></a>
                        </div>
                        <div class="tabs__content">
                            <div class="tabs__item tab1 active">
                                <table class="table_main table_main__no_heading">

                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__ch.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(中文)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__it.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Italiano)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__mag.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Magyar)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__rus.png" alt=""></i>
                                                <span>VEXA GLOBAL Marketing plan(Русский)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__spain.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Español)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__uk.png" alt=""></i>
                                                <span>VEXA GLOBALPresentation(English)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__vt.png" alt=""></i>
                                                <span>VEXA GLOBAL Marketing plan(Tiếng Việt)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__pl.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Polski)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__mag.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Magyar)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__rus.png" alt=""></i>
                                                <span>VEXA GLOBAL Marketing plan(Русский)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__spain.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Español)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__uk.png" alt=""></i>
                                                <span>VEXA GLOBALPresentation(English)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__vt.png" alt=""></i>
                                                <span>VEXA GLOBAL Marketing plan(Tiếng Việt)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="table_main__wrap">
                                                <i class="t_flag"><img src="img/flag/flag__pl.png" alt=""></i>
                                                <span>VEXA GLOBAL Presentation(Polski)</span>
                                            </div>
                                        </td>
                                        <td class="text-right"><div><a href="#">Download</a></div></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tabs__item tab2">

                                <div class="tabs__box">
                                    <div class="row">

                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 1</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__02.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 2</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__03.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 3</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__04.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 4</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 1</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__02.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 2</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__03.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 3</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bnr_image__04.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>BANNER 4</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tabs__item tab3">

                                <div class="tabs__box">
                                    <div class="row">

                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bonus__image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>PRESENTATION "CArS"</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/bonus__image__02.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>PRESENTATION "OFFICE"</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tabs__item tab4">

                                <div class="tabs__box">
                                    <div class="row">

                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>LOGO </span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>PLOGO round</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__03.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>Flag blue</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__04.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>Flag yellow</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>LOGO </span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>PLOGO round</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__03.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>Flag blue</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col col-xs-12 col-sm-6 col-md-4 col-xl-3 col-gutter-lr mb_40">
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="/images/brand_image__04.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="goods__name"><span>Flag yellow</span></div>
                                                <ul class="goods__btn">
                                                    <li>
                                                        <button type="button" class="btn btn_yellow"><span>OPEN SOURCE</span></button>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="btn btn_yellow btn_icon_cloud"><span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
