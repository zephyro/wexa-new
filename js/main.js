// Nav

(function() {

    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav_open');
    });

}());


// lng
(function() {

    $('.lng__active').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.lng').addClass('open');
    });


    $('.lng__dropdown a').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.lng').removeClass('open');
    });

}());

(function() {

    $('.currency__active').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.currency').toggleClass('open');
    });

    $('.currency__item').on('click touchstart', function(e){
        e.preventDefault();
        var box = $(this).closest('.currency');
        var icon = $(this).find('img').attr('src');
        var name = $(this).find('.currency__item_name').text();

        box.find('.currency__active_icon img').attr('src', icon);
        box.find('.currency__active_text span').text(name);

        box.removeClass('open');
    });

}());

// Filter Select

(function() {

    $('.filter_list__label').on('click', function(e){
        e.preventDefault();
        var checkOpen = $(this).closest('.filter_list').hasClass('open');

        if (checkOpen) {
            $(this).closest('.filter_list').removeClass('open');
        }
        else {
            $('.filter_list').removeClass('open');
            $(this).closest('.filter_list').addClass('open');
        }
    });


    $('.filter_list__label').on('click touchstart', function(e){

    });

}());

// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".lng").length === 0) {
        $(".lng").removeClass('open');
    }

    if ($(event.target).closest(".currency").length === 0) {
        $(".currency").removeClass('open');
    }

    if ($(event.target).closest(".filter_list").length === 0) {
        $(".filter_list").removeClass('open');
    }
});


$(".btn_modal").fancybox({
    'padding'    : 0
});

$(".informer__notice_scroll").mCustomScrollbar({
    theme:"minimal"
});

$(".table__content_scroll").mCustomScrollbar({
    theme:"minimal"
});

// Navigation
(function() {

    $(".nav_item")
        .mouseenter(function() {
            var point = $(this).attr("data-nav");
            var nav = '.nav_' + point;
            var bg = 'nav_item_' + point;
            $('.header_nav').find('.nav_item').removeClass('hover');
            $('.header_nav').find(nav).addClass('hover');
            $('.header_nav__hover').removeClass('nav_item_one').removeClass('nav_item_two').removeClass('nav_item_three').removeClass('nav_item_four');
            $('.header_nav__hover').addClass(bg);
        })
        .mouseleave(function(){
            $('.header_nav').find('.nav_item').delay(500).removeClass('hover');
            $('.header_nav__hover').removeClass('nav_item_01').removeClass('nav_item_02').removeClass('nav_item_03').removeClass('nav_item_04');
        });

}());

$('.form_select').selectric({
    //   maxHeight: 200
    disableOnMobile: true
});


(function() {

    $('.pack').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.pack_group').find('.pack').removeClass('active');
        $(this).addClass('active');
    });

    // Filter slider
    var $pack = $("#pack_slider");

    $pack.ionRangeSlider({
        skin: "round"
    });

    $pack.on("change", function () {
        var $inp = $(this);
        var from = '$' + $inp.prop("value");

        $('.pack_range__value').text(from);
    });


}());

// Tabs

(function() {

    $('.tabs__nav a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.tabs__item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

$(function() {
    $(".table_sort").tablesorter();
});

(function() {

    $('.tree_table').treeTable({
        ignoreClickOn: "input, a, img"
    });

}());
