<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content">
                        <div class="content__header">
                            <h2>Refill</h2>
                        </div>

                        <form class="form">

                            <div class="refill">
                                <div class="refill__left">
                                    <div class="form_group">
                                        <div class="form_label">Cryptocurrency</div>
                                        <div class="currency">
                                            <div class="currency__active">
                                                <div class="currency__active_icon">
                                                    <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                </div>
                                                <div class="currency__active_text"><span>Bitcoin</span></div>
                                                <input class="currency__value" type="hidden" name="currency" value="Bitcoin">
                                            </div>
                                            <div class="currency__list">
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__btc.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Bitcoin</div>
                                                </div>
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__ltc.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Litecoin</div>
                                                </div>
                                                <div class="currency__item">
                                                    <div class="currency__item_icon">
                                                        <img src="img/currency__e.png" class="img-fluid" alt="">
                                                    </div>
                                                    <div class="currency__item_name">Ethereum</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_group mb_0">
                                        <div class="form_label">Please send BTC to this address</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="3FwMiN3rxkeVXfaoLpLJz1WS5koWRpcW56" disabled>
                                    </div>
                                </div>
                                <div class="refill__center">
                                    <div class="refill__qr">
                                        <img src="images/qr2.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="refill__right">
                                    <div class="form_group">
                                        <div class="form_label">Amount, USD</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="4324">
                                    </div>
                                    <div class="form_group">
                                        <div class="form_label">Amount, BTC</div>
                                        <input class="form_control" type="text" name="amount" placeholder="" value="0.46704433" disabled>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
