<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>My packages</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row pack_group">

                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack pack_small active" data-name="basic" data-days="130">
                                <div class="pack__top mb_0">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_01.png" class="img-fluid" alt=""></i>
                                        <span>BASIC</span>
                                    </div>
                                    <div class="pack__period mb_0"><span>130 business days</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack pack_small" data-name="premium" data-days="150">
                                <div class="pack__top mb_0">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_02.png" class="img-fluid" alt=""></i>
                                        <span>premium</span>
                                    </div>
                                    <div class="pack__period mb_0"><span>150 business days</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack pack_small" data-name="exclusive" data-days="180">
                                <div class="pack__top mb_0">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_03.png" class="img-fluid" alt=""></i>
                                        <span>EXCLUSIVE</span>
                                    </div>
                                    <div class="pack__period mb_0"><span>180 business days</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-gutter-lr">
                            <div class="content_box">
                                <table class="table_main">
                                    <thead>
                                    <tr>
                                        <th class="text-uppercase">
                                            <div class="table_main__wrap">
                                                <i><img src="img/icon__table_ticket.png" alt=""></i>
                                                <span>ID</span>
                                            </div>
                                        </th>
                                        <th class="text-uppercase">
                                            <div class="table_main__wrap">
                                                <i><img src="img/icon__table_money.png" alt=""></i>
                                                <span>AMOUNT</span>
                                            </div>
                                        </th>
                                        <th class="text-uppercase">
                                            <div class="table_main__wrap">
                                                <i><img src="img/icon__table_dollar.png" alt=""></i>
                                                <span>INCOME</span>
                                            </div>
                                        </th>
                                        <th class="text-uppercase">
                                            <div class="table_main__wrap">
                                                <i><img src="img/icon__table_balance.png" alt=""></i>
                                                <span>TOTAL INCOME</span>
                                            </div>
                                        </th>
                                        <th class="text-uppercase">
                                            <div class="table_main__wrap">
                                                <i><img src="img/icon__table_date.png" alt=""></i>
                                                <span>DATE</span>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    <tr>
                                        <td data-header="ID"><div>7758825</div></td>
                                        <td data-header="AMOUNT"><div>$15,899</div></td>
                                        <td data-header="INCOME"><div>$150,00</div></td>
                                        <td data-header="TOTAL INCOME" class="text-nowrap"><div>$150,00</div></td>
                                        <td data-header="DATE" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
