body {
	position: relative;
	
	&:before {
		top: 61px;
		left: 0;
		right: 0;
		bottom: 0;
		z-index: 2;
		content: '';
		opacity: 0;
		visibility: hidden;
		height: 46px;
		display: block;
		position: absolute;
		border-bottom: 1px solid #19273c;
		background: url("../img/nav__bg.png") 50% 100% repeat-x;
	}
	
	@media (min-width: $screen-lg-min) {
		
		&:before {
			opacity: 1;
			visibility: visible;
			@include box-shadow(0px 2px 5px 0px rgba(0, 0, 0, 0.2));
		}
	}
}

.page {
	width: 100%;
	padding-top: 61px;
	overflow: hidden;
	position: relative;
	min-height: 100vh;
	@include flexbox();
	@include flex-direction(column);
	background: url("../img/bottom__bg.png") 50% 100% no-repeat;
	
	&:after {
		top: 0;
		left: 0;
		right: 0;
		z-index: 800;
		content: '';
		height: 61px;
		display: block;
		position: fixed;
		border-bottom: 1px solid #1e2f48;
		background: url("../img/header__bg.png") 50% 0 repeat-x;
		@include box-shadow(0px 2px 5px 0px rgba(0, 0, 0, 0.2));
	}
	
	&:before {
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		z-index: 801;
		content: '';
		opacity: 0.5;
		height: 60px;
		display: block;
		position: fixed;
		background: url("../img/header__light.png") 50% 100% no-repeat;
	}
	
	@media (min-width: $screen-lg-min) {
		padding-top: 110px;
		
		&:after {
			z-index: 1;
			position: absolute;
			@include box-shadow(none);
		}
		
		&:before {
			z-index: 2;
			position: absolute;
		}
	}
}

.main {
	z-index: 100;
	position: relative;
	padding-top: 40px;
	padding-bottom: 80px;
	@include flex-grow(1);
	
	@media (min-width: $screen-lg-min) {
		padding-top: 60px;
		padding-bottom: 100px;
	}
	
	@media (min-width: $screen-xl-min) {
		padding-top: 40px;
	}
}

.heading {
	margin-bottom: 35px;
	
	h1 {
		padding: 5px 5px 5px 0;
	}
	
	&__time {
		color: #637b9b;
		font-size: 1.3rem;
		line-height: 2rem;
		padding-left: 28px;
		white-space: nowrap;
		background: url("../img/icon__time.png") 0 50% no-repeat;
		
		strong {
			color: #fff;
			font-weight: 400;
		}
		
	}
	
	&__left {
		margin-bottom: 10px;
		@include flexbox();
		@include flex-wrap(wrap);
		@include align-items(center);
	}
	
	&__right {
	
	}
	
	@media (min-width: 567px) {
	
		h1 {
			margin-right: 30px;
		}
	}
	
	@media (min-width: $screen-sm-min) {
	
	}
	
	@media (min-width: $screen-md-min) {
		@include flexbox();
		@include align-items(center);
		@include justify-content(space-between);
		
		&__left {
			margin-bottom: 0;
		}
		
		&__right {
			@include flexbox();
		}
	}
}

.user_status {
	height: 28px;
	margin-left: 10px;
	font-size: 1.1rem;
	font-weight: 700;
	line-height: 1.8rem;
	white-space: nowrap;
	position: relative;
	display: inline-block;
	padding: 5px 10px 5px 30px;
	text-transform: uppercase;
	@include flex-shrink(0);
	@include border-radius(14px);
	
	&:before {
		top: 6px;
		left: 10px;
		content: '';
		width: 16px;
		height: 16px;
		position: absolute;
		display: inline-block;
		@include border-radius(50%);
	}
	
	&__verified {
		color: #fff;
		background: #71ca1f;
		
		&:before {
			background: #fff url("../img/icon__ok.svg") 50% 50% no-repeat;
		}
	}
	
	&__processing {
		color: #363636;
		background: #aeb2c5;
		
		&:before {
			background: #fff url("../img/icon__processing.png") 50% 50% no-repeat;
		}
	}
	
	&__unverified {
		color: #fff;
		background: #e43443;
		
		&:before {
			background: #fff url("../img/icon__unverified.png") 50% 50% no-repeat;
		}
	}
}

.ref {
	color: #637b9b;
	font-size: 1.3rem;
	line-height: 2rem;
	max-width: 510px;
	padding: 4px 4px 4px 25px;
	border: 1px solid #2a3340;
	background: url("../img/icon__link.png") 8px 50% no-repeat;
	@include flexbox();
	@include border-radius(4px);
	@include align-items(center);
//	@include flex-wrap(wrap);
	
	&__label {
		font-size: 0;
		line-height: 0;
		white-space: nowrap;
		padding: 0 5px;

	}
	
	&__link {
		width: 100%;
		color: #fff;
		border: none;
		outline: none;
		padding: 3px 0;
		font-weight: 600;
		font-size: 1.3rem;
		line-height: 2rem;
		overflow: hidden;
		display: inline-block;
		background: transparent;
		padding-right: 10px;
	}

	&__button {
		display: none;
		padding-left: 33px;
		background-repeat: no-repeat;
		background-position: 12px 50%;
		background-image: url("../img/icon__copy.png");
		@include flex-shrink(0);
	}
	
	@media (min-width: 567px) {
		width: 510px;
		@include flex-wrap(nowrap);
		
		&__button {
			display: inline-block;
		}
	}

	@media (min-width: $screen-sm-min) {

		&__label {
			font-size: 12px;
			line-height: 20px;
		}
	}
	
	@media (min-width: $screen-md-min) {
		width: 480px;

	}

	@media (min-width: $screen-lg-min) {
		width: 450px;
	}
	
	@media (min-width: $screen-xl-min) {
		width: 510px;
	}
}

.content {
	padding: 20px 20px 30px 20px;
	background: rgba(119,178,255,0.05);
	@include box-shadow(0px 2px 5px 0px rgba(0, 0, 0, 0.05));
	@include border-radius(4px);

	&__header {
		margin-bottom: 25px;
		@include flexbox();
		@include align-items(center);
		@include justify-content(space-between);

		h2 {
			padding: 5px 0;
		}
	}

	@media (min-width: $screen-sm-min) {
		padding: 30px 40px 40px 40px;
	}

	@media (min-width: $screen-md-min) {

	}

	@media (min-width: $screen-lg-min) {

	}

	@media (min-width: $screen-xl-min) {

	}
}

.content_box {
	color: #fff;
	font-size: 1.4rem;
	line-height: 2.4rem;
	padding: 20px 20px 35px 20px;
	background: rgba(119,178,255,0.05);
	@include box-shadow(0px 2px 5px 0px rgba(0, 0, 0, 0.05));
	@include border-radius(4px);

	&_height {
		height: 100%;
	}

	&__heading {
		margin-top: -5px;
		margin-bottom: 25px;
		@include flexbox();
		@include flex-wrap(wrap);
		@include align-items(center);
		@include justify-content(space-between);

		h3 {
			padding: 5px 0;
		}
	}

	&__period {
		color: #fff;
		font-weight: 600;
		font-size: 1.2rem;
		line-height: 2rem;
		padding-left: 26px;
		white-space: nowrap;
		display: inline-block;
		background: url("../img/icon__clock.png") 0 50% no-repeat;
	}

	&__top {
		padding: 0;
		z-index: 50;
		position: relative;
		list-style: none;
		margin: -5px -5px 25px -5px;
		@include flexbox();
		@include flex-wrap(wrap);
		@include align-items(center);
		@include justify-content(space-between);

		> li {
			padding: 5px;
		}
	}

	@media (min-width: $screen-sm-min) {
		padding: 20px 40px 35px 40px;

		&__table {
	
		}
	}

}


.status {
	color: #fff;
	font-size: 1rem;
	font-weight: 700;
	line-height: 1.4rem;
	position: relative;
	display: inline-block;
	text-transform: uppercase;
	padding: 5px 10px 5px 25px;
	@include border-radius(12px);

	&:before {
		top: 50%;
		left: 5px;
		width: 14px;
		height: 14px;
		content: '';
		margin-top: -7px;
		position: absolute;
		display: inline-block;
		background-color: #fff;
		background-position: 50% 50%;
		background-repeat: no-repeat;
		@include border-radius(50%);
	}

	&_open {
		background: rgba(11,50,178,0.2);

		&:before {
			background-image: url("../img/status__open.png");
		}
	}

	&_closed {
		background: rgba(62,126,11,0.2);

		&:before {
			background-image: url("../img/status__open.png");
		}
	}

	&_processing {
		background: rgba(132,136,154,0.2);

		&:before {
			background-image: url("../img/status_processing.png");
		}
	}

	&_answer {
		background: rgba(243,177,0,0.2);

		&:before {
			background-image: url("../img/status_answer.png");
		}
	}
}

.qr {
	padding: 30px 20px;
	margin-bottom: 30px;
	border: 1px solid #2e3a4b;
	@include border-radius(6px);
	@include flexbox();
	@include flex-direction(column);

	&__wrap {
		margin: auto 0;
	}

	&__item {
		max-width: 252px;
		margin: 0 auto;
	}

	@media (min-width: $screen-md-min) {
		height: 100%;
		margin-bottom: 0;

		&__item {
			width: 252px;
		}
	}
}

.qr_sm {
	margin-bottom: 25px;

	&__item {
		width: 187px;
		margin: 0 auto;
	}
}

.currency {
	z-index: 100;
	font-size: 0;
	line-height: 0;
	display: block;
	position: relative;

	&__active {
		cursor: pointer;
		position: relative;
		padding: 9px 40px 9px 15px;
		border: 1px solid #384659;
		@include flexbox();
		@include align-items(center);
		@include border-radius(4px);

		&:before {
			top: 50%;
			right: 0;
			width: 36px;
			height: 11px;
			content: '';
			margin-top: -6px;
			position: absolute;
			display: inline-block;
			background: url("../img/icon__select.png") 0 0 no-repeat;
			@include transition(.3s);
		}

		&_icon {
			width: 20px;
			height: 20px;
			display: block;
			margin-right: 8px;
			position: relative;
			@include flex-shrink(0);
		}

		&_text {
			color: #ffffff;
			font-weight: 600;
			font-size: 1.3rem;
			line-height: 2rem;
			position: relative;
			@include flex-grow(1);

			span {
				display: block;
				max-width: 100%;
				overflow: hidden;
				text-overflow: ellipsis;
			}
		}
	}

	&__list {
		left: 0;
		top: 100%;
		width: 100%;
		opacity: 0;
		z-index: -1;
		border: none;
		overflow: hidden;
		margin-top: 1px;
		max-width: 100%;
		visibility: hidden;
		position: absolute;
		background: #345784;
		@include box-shadow(0px 12px 14px 0px rgba(0, 0, 0, 0.25));
		@include border-radius(4px);
		@include transition(.3s);
	}

	&__item {
		color: #fff;
		height: 36px;
		cursor: pointer;
		padding: 8px 11px;
		@include flexbox();
		@include align-items(center);

		&:hover {
			background: #1e324e;
		}

		&_icon {
			width: 20px;
			height: 20px;
			display: block;
			margin-right: 8px;
			position: relative;
			@include flex-shrink(0);
		}

		&_name {
			color: #fff;
			font-weight: 600;
			font-size: 1.3rem;
			line-height: 2rem;
			position: relative;
			@include flex-grow(1);

			span {
				display: block;
				max-width: 100%;
				overflow: hidden;
				text-overflow: ellipsis;
			}
		}
	}

	&.open {
		z-index: 200;

		.currency__active {

		}

		.currency__list {
			opacity: 1;
			z-index: 100;
			visibility: visible;
			@include transition(.3s);
		}
	}
}

.main_status {
	color: #fff;
	font-size: 1.1rem;
	font-weight: 700;
	line-height: 2rem;
	position: relative;
	display: inline-block;
	text-transform: uppercase;
	padding: 5px 10px 5px 30px;
	@include border-radius(15px);

	&:before {
		top: 50%;
		left: 7px;
		width: 16px;
		height: 16px;
		content: '';
		margin-top: -8px;
		position: absolute;
		display: inline-block;
		background-color: #fff;
		background-position: 50% 50%;
		background-repeat: no-repeat;
		@include border-radius(50%);
	}

	&__bad {
		background: #b33c3e;

		&:before {
			background-image: url("../img/main_status__bad.png");
		}
	}
}


.styled {
	margin: 0;
	padding: 0;
	color: #fff;
	font-size: 1.2rem;
	line-height: 2rem;
	list-style: none;

	> li {
		padding-left: 20px;
		position: relative;
		margin-bottom: 10px;

		&:last-child {
			margin-bottom: 0;
		}

		&:before {
			left: 0;
			top: 6px;
			width: 8px;
			height: 8px;
			content: '';
			background: #fef852;
			position: absolute;
			display: inline-block;
			@include border-radius(50%);
			@include box-shadow(0px 0px 10px 0px rgba(34, 83, 245, 1));
		}
	}
}

.drop_files {
	width: 100%;
	height: 146px;
	color: #687a98;
	font-weight: 600;
	font-size: 1.2rem;
	line-height: 2rem;
	text-align: center;
	padding-top: 110px;
	margin-bottom: 15px;
	text-transform: uppercase;
	border: 2px solid #3f8ff7;
	background: url("../img/drop_file__icon.png") 50% 18px no-repeat;
	@include border-radius(4px);

	&__uploaded {
		@include flexbox();
		@include flex-wrap(wrap);
	}

	&__item {
		color: #fff;
		font-size: 1.2rem;
		line-height: 2rem;
		margin-right: 30px;
		position: relative;
		padding-left: 25px;
		display: inline-block;
		background: url('../img/icon__file.png') 0 50% no-repeat;
	}
}

.message {
	color: #fff;
	font-size: 1.2rem;
	line-height: 1.8rem;
	padding: 6px 0 6px 10px;
	border-left: 2px solid #fff800;
}

.direct_email {
	color: #637b9b;
	font-size: 1.3rem;
	line-height: 2rem;
//	max-width: 400px;
	padding: 4px 10px 4px 4px;
	border: 1px solid #2a3340;
	@include flexbox();
	@include border-radius(4px);
	@include align-items(center);
	//	@include flex-wrap(wrap);

	&__label {
		white-space: nowrap;
		padding: 0 5px 0 30px;
		background: url("../img/icon__direct_email.png") 3px 50% no-repeat;
	}

	&__link {
		width: 100%;
		color: #fff;
		border: none;
		outline: none;
		padding: 3px 0;
		font-weight: 600;
		font-size: 1.3rem;
		line-height: 2rem;
		overflow: hidden;
		display: inline-block;
		background: transparent;
		padding-right: 10px;
	}
}

.content_filter {
	z-index: 50;
	position: relative;
	margin: -5px -5px 25px -5px;

	&__left {
		margin: 0;
		padding: 0;
		list-style: none;

		> li {
			padding: 5px;
		}
	}

	&__right {
		margin: 0;
		padding: 0;
		list-style: none;

		> li {
			padding: 5px;
		}
	}

	@media (min-width: $screen-sm-min) {
		@include flexbox();
		@include align-items(center);
		@include justify-content(space-between);

		&__left {
			@include flexbox();
			@include flex-grow(1);
			@include align-items(center);

			> li {
				@include flex-grow(1);
			}
		}

		&__right {
			@include flex-shrink(0);
		}
	}

	@media (min-width: $screen-lg-min) {

		&__left {
			max-width: 700px;

			> li {
				margin-right: 40px;
				@include flex-grow(1);
			}
		}
	}
}


.filter {
	width: 100%;

	&__legend {
		display: block;
		color: #fff800;
		font-weight: 300;
		font-size: 1.2rem;
		line-height: 1.6rem;
		margin-bottom: 5px;
		white-space: nowrap;
	}

	&__content {

	}

	@media (min-width: $screen-sm-min) {
		@include flexbox();
		@include align-items(center);

		&__legend {
			margin: 0;
			color: #fff;
			height: 40px;
			padding: 12px 10px;
			border: 1px solid #4d5561;
			background: rgba(255,255,255,0.1);
			border-right: none;
			@include border-radius(3px 0 0 3px);
		}

		&__content {
			@include flex-grow(1);

			.filter_list__label {
				border-color: #4d5561;
				@include border-radius(0 3px 3px 0);
			}
		}
	}

	@media (min-width: $screen-md-min) {

	}
}


.filter_list {
	width: 100%;
	z-index: 50;
	position: relative;

	&__label {
		margin: 0;
		width: 100%;
		height: 40px;
		outline: none;
		color: #ffffff;
		cursor: pointer;
		position: relative;
		font-size: 1.3rem;
		line-height: 2rem;
		padding: 9px 40px 9px 15px;
		display: inline-block;
		vertical-align: middle;
		background: rgba(255,255,255,0.05);
		border: 1px solid #384659;
		@include border-radius(4px);

		&:before {
			top: 0;
			right: 0;
			width: 36px;
			height: 100%;
			font-size: 0;
			line-height: 0;
			display: block;
			content: '';
			text-align: center;
			position: absolute;
			background: transparent url("../img/icon__select.png") 6px 50% no-repeat;
		}
	}

	&__dropdown {
		left: 0;
		z-index: -1;
		width: 100%;
		padding: 5px 0;
		display: none;
		max-width: 100%;
		overflow: hidden;
		position: absolute;
		background: #1e324e;
		top: calc(100% + 1px);
		@include box-shadow(0px 12px 14px 0px rgba(0, 0, 0, 0.25));
		@include border-radius(4px);
	}

	&__checkbox {
		display: block;
		font-size: 0;
		line-height: 0;
		font-weight: 400;
		position: relative;

		input {
			top: 0;
			left: 0;
			width: 1px;
			height: 1px;
			opacity: 0;
			overflow: hidden;
			visibility: hidden;
			position: absolute;
		}

		span {
			display: block;
			cursor: pointer;
			font-size: 1.3rem;
			line-height: 2rem;
			padding: 5px 13px 5px 42px;

			&:before {
				top: 7px;
				left: 13px;
				content: '';
				z-index: 10;
				width: 16px;
				height: 16px;
				position: absolute;
				display: inline-block;
				border: 1px solid #ffcc00;
			}

			&:after {
				top: 7px;
				left: 13px;
				content: '';
				z-index: 20;
				width: 16px;
				height: 16px;
				opacity: 0;
				visibility: hidden;
				position: absolute;
				display: inline-block;
				background: url("../img/icon__check.png") 50% 50% no-repeat;
			}
		}

		input:checked + span {

			&:before {
				background-color: #ffcc00;
			}

			&:after {
				opacity: 1;
				visibility: visible;
				@include transition(.3s);
			}
		}
	}

	&.open {
		z-index: 100;

		.filter_list__dropdown {
			z-index: 100;
			display: block;
		}
	}
}

.refill {
	@include flexbox();
	@include flex-direction(column);

	&__center {
		padding: 40px 1px;
		border: 1px solid #2e3a4b;
		margin-bottom: 40px;
		@include order(1);
	}

	&__qr {
		width: 187px;
		margin: 0 auto;
	}

	&__left {
		@include order(2);
	}

	&__right {
		@include order(3);
	}

	@media (min-width: $screen-md-min) {
		@include flex-direction(row);

		&__center {
			padding: 0;
			width: 240px;
			border: none;
			margin-bottom: 0;
			@include order(2);
			@include flex-shrink(0);
		}

		&__qr {
			margin-top: -50px;
		}

		&__left {
			@include order(1);
			@include flex-grow(1);
		}

		&__right {
			@include order(3);
			@include flex-grow(1);
		}
	}


	@media (min-width: $screen-lg-min) {

		&__center {
			width: 320px;
		}
	}
}


.pagination {
	padding: 25px 0 0 0;
	margin: 0 -3px;
	list-style: none;
	@include flexbox();
	@include align-items(center);
	@include justify-content(center);

	> li {
		color: #fff;
		width: 28px;
		height: 28px;
		margin: 0 3px;
		display: block;
		text-align: center;

		a {
			color: #fff;
			width: 28px;
			height: 28px;
			display: block;
			padding: 5px 0;
			font-weight: 600;
			font-size: 1.2rem;
			line-height: 1.8rem;
			text-align: center;
			cursor: pointer;
			text-decoration: none;
			background: rgba(119,178,255,0.15);
			@include transition(.3s);

			&:hover {
				color: #fff;
				text-decoration: none;
				background: rgba(119,178,255,0.45);
				@include transition(.3s);
			}
		}

		&.active {

			a {
				background: rgba(119,178,255,0.45);
			}
		}
	}

	@media (min-width: $screen-sm-min) {
		@include justify-content(flex-end);
	}
}

.message {
	font-weight: 600;
	font-size: 1.3rem;
	line-height: 2rem;
	border-left: 2px solid $color-blue;
	padding: 10px 10px 10px 35px;
	background: rgba(119, 178, 255, 0.1);

	&_blue {
		color: #0a5e8a;
		border-left-color: #0a5e8a;
		background: rgba(119, 178, 255, 0.1) url("../img/icon__alert_blue.png") 12px 12px no-repeat;
	}

	&_red {
		color: #ff384b;
		border-left-color: #ff384b;
		background: rgba(119, 178, 255, 0.1) url("../img/icon__alert_red.png") 12px 12px no-repeat;
	}


	&_yellow {
		color: #fff;
		border-left-color: #e0b40a;
		background: rgba(119, 178, 255, 0.1) url("../img/icon__alert_yellow.png") 12px 50% no-repeat;

		.btn {
			margin: 0 8px;

			&:before {
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
			}
		}
	}

}



@media (min-width: $screen-sm-min) {

}

@media (min-width: $screen-md-min) {

}

@media (min-width: $screen-lg-min) {

}

@media (min-width: $screen-xl-min) {

}