<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row pack_group">

                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack active" data-name="basic" data-days="130">
                                <div class="pack__top">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_01.png" class="img-fluid" alt=""></i>
                                        <span>BASIC</span>
                                    </div>
                                    <div class="pack__value">$50-$999</div>
                                </div>
                                <div class="pack__bottom">
                                    <div class="pack__period"><span>130 business days</span></div>
                                    <a href="#" class="btn btn_yellow btn_text_sm" title="Buy basic"><span>Buy basic</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack" data-name="premium" data-days="150">
                                <div class="pack__top">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_02.png" class="img-fluid" alt=""></i>
                                        <span>premium</span>
                                    </div>
                                    <div class="pack__period"><span>150 business days</span></div>
                                </div>
                                <div class="pack__bottom">
                                    <div class="pack__value">$1,000-$9,999</div>
                                    <a href="#" class="btn btn_yellow btn_text_sm" title="Buy premium"><span>Buy premium</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-sm-4 col-gutter-lr">
                            <div class="pack" data-name="exclusive" data-days="180">
                                <div class="pack__top">
                                    <div class="pack__name">
                                        <i><img src="img/pack__icon_03.png" class="img-fluid" alt=""></i>
                                        <span>EXCLUSIVE</span>
                                    </div>
                                    <div class="pack__period"><span>180 business days</span></div>
                                </div>
                                <div class="pack__bottom">
                                    <div class="pack__value">$10,00-$100,000</div>
                                    <a href="#" class="btn btn_yellow btn_text_sm" title="Buy exclusive"><span>Buy exclusive</span></a>
                                </div>
                            </div>
                        </div>

                        <div class="col col-xs-12 col-lg-6 col-gutter-lr mb_40">
                            <div class="content_box content_box_height">
                                <div class="content_box__heading">
                                    <h3 class="text-uppercase">VEXAGLOBAL <span class="color_yellow">EXCLUSIVE</span></h3>
                                    <div class="content_box__period">180 business days</div>
                                </div>

                                <div class="pack_range">
                                    <div class="pack_range__slider">
                                        <input type="hidden" id="pack_slider" name="pack_slider" value=""
                                               data-type="single"
                                               data-min="10000"
                                               data-max="100000"
                                               data-from="72560"
                                               data-grid="false"
                                               data-step="10"
                                               data-prefix="$"
                                        />
                                    </div>
                                    <div class="pack_range__grid">
                                        <span>$10.000</span>
                                        <span>$100.000</span>
                                    </div>
                                    <div class="pack_range__total">
                                        <div class="pack_range__value">$72560</div>
                                        <ul class="pack_range__summary">
                                            <li>
                                                <span>DAILY PROFIT</span>
                                                <strong>$1000.00</strong>
                                            </li>
                                            <li>
                                                <span>TOTAL RETURN</span>
                                                <strong>$180000.00</strong>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col col-xs-12 col-lg-6 col-gutter-lr  mb_40">
                            <div class="content_box content_box_height">
                                <div class="content_box__heading">
                                    <h3>Transfer between accounts</h3>
                                </div>
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Amount</label>
                                            <input type="text" class="form_control" name="n1" placeholder="560,00">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Select balance</label>
                                            <select class="form_control form_select" name="s1">
                                                <option value="Main balance">Main balance</option>
                                                <option value="Fund balance">Fund balance</option>
                                                <option value="Affiliate balance">Affiliate balance</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <button type="submit" class="btn btn_yellow btn__money btn_long"><span>BUY PACKAGE</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
