<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Page</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="row">

                        <div class="col col-xs-12 col-lg-6">

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Account info</h3>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Email</label>
                                    <div class="form_inline">
                                        <input class="form_control form_control__email" type="text" name="email" placeholder="info3@vexaglobal.com" value="">
                                        <button type="button" class="form_inline__btn form_inline__change"></button>
                                    </div>
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Nickname</label>
                                    <div class="form_inline">
                                        <input class="form_control form_control__user" type="text" name="email" placeholder="Test" value="">
                                        <button type="button" class="form_inline__btn form_inline__save"></button>
                                    </div>
                                </div>
                            </div>

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Cryptocurrency wallets</h3>
                                </div>
                                <div class="form_group">
                                    <label class="form_label form_label_icon">
                                        <i><img src="img/label__btc.png" alt=""></i>
                                        <span>Bitcoin</span>
                                    </label>
                                    <div class="form_inline">
                                        <input class="form_control form_control__wallet" type="text" name="email" placeholder="info3@vexaglobal.com" value="">
                                        <button type="button" class="form_inline__btn form_inline__save"></button>
                                    </div>
                                </div>
                            </div>

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Personal information</h3>
                                </div>
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Name</label>
                                            <input class="form_control" type="text" name="n1" placeholder="" value="Nick">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Surname</label>
                                            <input class="form_control" type="text" name="n2" placeholder="" value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Date of birth</label>
                                            <input class="form_control" type="text" name="n3" placeholder="" value="2019 - 08 - 01">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Document ID</label>
                                            <input class="form_control" type="text" name="n4" placeholder="" value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Country</label>
                                            <input class="form_control" type="text" name="n5" placeholder="" value="Belize">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">City</label>
                                            <input class="form_control" type="text" name="n6" placeholder="" value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Address</label>
                                            <input class="form_control" type="text" name="n7" placeholder="" value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Zip</label>
                                            <input class="form_control" type="text" name="n8" placeholder=""  value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Tax ID</label>
                                            <input class="form_control" type="text" name="n7" placeholder="" value="Test">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <button type="button" class="btn btn_yellow btn_long btn_save"><span>Save info</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col col-xs-12 col-lg-6">

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Account Turn on Google Authenticator</h3>
                                </div>
                                <div class="form_group">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="ch1" value="1">
                                        <span>Turn on Google Authenticator</span>
                                    </label>
                                </div>
                            </div>

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Password change form</h3>
                                </div>
                                <div class="row">
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">New password</label>
                                            <input class="form_control" type="password" name="p1" placeholder="• • • • • • • •" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Current password</label>
                                            <input class="form_control" type="password" name="p2" placeholder="• • • • • • • •" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Current password</label>
                                            <input class="form_control" type="password" name="p3" placeholder="• • • • • • • •" value="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label"></label>
                                            <button type="submit" class="btn btn_yellow btn_long btn_edit"><span>Change password</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Verification</h3>
                                    <div class="main_status main_status__bad">Unverified</div>
                                </div>
                                <p class="color_blue">For verification please upload:</p>
                                <ul class="styled mb_40">
                                    <li>Copy of your ID Card(both sides) or Passport or Driver license</li>
                                    <li>Selfie photo with your ID Card or Passport or Driver license</li>
                                    <li>Optional. Document for address confirmation(utility bill or bank statment)</li>
                                    <li>Photo on which you are holding paper with the inscription "Vexa Verification"</li>
                                </ul>

                                <div class="drop_files">
                                    <div class="drop_files__text">Drop files here to upload</div>
                                </div>
                                <div class="drop_files__uploaded">
                                    <span class="drop_files__item">Bitcoin-Symbol.png</span>
                                    <span class="drop_files__item">photo_2019-08-15_19-27-49.jpeg</span>
                                </div>

                            </div>

                            <div class="content_box mb_40">
                                <div class="content_box__heading">
                                    <h3>Subscription</h3>
                                </div>
                                <div class="group_inline">
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch">
                                        <span>News subscription</span>
                                    </label>
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch" checked="">
                                        <span> Ticket alert subscription</span>
                                    </label>
                                    <label class="form_switch">
                                        <input type="checkbox" name="switch">
                                        <span>Subscription to letters of payments</span>
                                    </label>
                                </div>
                            </div>

                        </div>

                    </div>



                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
