<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Direct partners</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content_box">
                        <ul class="content_box__top">
                           <li>
                               <div class="direct_email">
                                   <div class="direct_email__label">Direct partner email:</div>
                                   <input class="direct_email__link" type="text" name="ref" value="system@vexaglobal.com" disabled="">
                               </div>
                           </li>
                            <li>
                                <div class="form_line">
                                    <input class="form_line__input" type="text" name="email" placeholder="Partner ID" value="">
                                    <button type="button" class="btn btn_yellow btn_sm btn_no_shadow btn_search"><span>SEARCH</span></button>
                                </div>
                            </li>
                        </ul>

                        <table class="table_main tree_table">
                            <thead>
                            <tr>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_name.png" alt=""></i>
                                        <span>NAME</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_line.png" alt=""></i>
                                        <span>Line</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_partner.png" alt=""></i>
                                        <span>PARTNERS</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_level.png" alt=""></i>
                                        <span>LEVEL</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_package.png" alt=""></i>
                                        <span>PACKAGES</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_sale.png" alt=""></i>
                                        <span>SALES</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_link.png" alt=""></i>
                                        <span>REFFERAL LINK</span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr data-level="0">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>


                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>


                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="2">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>


                            <tr data-level="0">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="0">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="0">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>


                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>
                            <tr data-level="1">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            <tr data-level="0">
                                <td data-header="NAME"><div>8t38631</div></td>
                                <td data-header="LINE"><div>34</div></td>
                                <td data-header="PARTNERS"><div>144</div></td>
                                <td data-header="LEVEL"><div>28</div></td>
                                <td data-header="PACKAGES"><div>$200,00</div></td>
                                <td data-header="SALES"><div>$120,00</div></td>
                                <td data-header="REFFERAL LINK"><div>https://vexaglobal.com/r/VX581975/VX49X49595445712</div></td>
                            </tr>

                            </tbody>
                        </table>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
