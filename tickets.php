<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Tickets</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>


                    <div class="content">
                        <div class="content__header">
                            <h2>Tickets list</h2>
                            <a href="#" class="btn btn_yellow btn_create"><span>CREATE TICKET</span></a>
                        </div>
                        <table class="table_main">
                            <thead>
                            <tr>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_ticket.png" alt=""></i>
                                        <span>Ticket Id</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_subject.png" alt=""></i>
                                        <span>SUBJECT</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_wait.png" alt=""></i>
                                        <span>STATUS</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_create.png" alt=""></i>
                                        <span>CREATED</span>
                                    </div>
                                </th>
                                <th class="text-uppercase">
                                    <div class="table_main__wrap">
                                        <i><img src="img/icon__table_clock.png" alt=""></i>
                                        <span>LAST REPLY</span>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-header="Ticket Id"><div>AL492683934</div></td>
                                <td data-header="SUBJECT" class="color_blue text-uppercase"><div>Web Development | HTML5 | C</div></td>
                                <td data-header="STATUS"><div><span class="status status_open">OPENED</span></div></td>
                                <td data-header="CREATED" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                <td data-header="LAST REPLY" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                            </tr>
                            <tr>
                                <td data-header="Ticket Id"><div>TR492683911</div></td>
                                <td data-header="SUBJECT" class="color_blue text-uppercase"><div>Web Development | HTML5 | CSS3 | WordPress</div></td>
                                <td data-header="STATUS"><div><span class="status status_closed">OPENED</span></div></td>
                                <td data-header="CREATED" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                <td data-header="LAST REPLY" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                            </tr>
                            <tr>
                                <td data-header="Ticket Id"><div>TR492683911</div></td>
                                <td data-header="SUBJECT" class="color_blue text-uppercase"><div>Web Development | HTML5 | CSS3 | WordPress</div></td>
                                <td data-header="STATUS"><div><span class="status status_processing">Processing</span></div></td>
                                <td data-header="CREATED" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                <td data-header="LAST REPLY" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                            </tr>
                            <tr>
                                <td data-header="Ticket Id"><div>TR492683911</div></td>
                                <td data-header="SUBJECT" class="color_blue text-uppercase"><div>Web Development | HTML5 | CSS3 | WordPress</div></td>
                                <td data-header="STATUS"><div><span class="status status_answer">user`s  answer</span></div></td>
                                <td data-header="CREATED" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                                <td data-header="LAST REPLY" class="text-nowrap"><div>2019-08-21 06:23:49</div></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
