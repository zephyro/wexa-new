<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <?php include('inc/nav.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__left">
                            <h1>Career</h1>
                            <div class="heading__status">
                                <span class="user_status user_status__verified">Verified</span>
                                <span class="user_status user_status__processing">Processing</span>
                                <span class="user_status user_status__unverified">Unverified</span>
                            </div>
                        </div>
                        <div class="heading__right">
                            <div class="ref">
                                <div class="ref__label">Refferal link:</div>
                                <input class="ref__link" type="text" name="ref" value="https://vexaglobal.com/r/VX571207/VX571207" disabled>
                                <button type="button" class="btn_sm ref__button">Copy</button>
                            </div>
                        </div>
                    </div>

                    <?php include('inc/board.inc.php') ?>

                    <div class="career career_success">
                        <div class="career__step">
                            <div class="career__step_number">
                                <strong>1</strong>
                                <span>LEVEL</span>
                            </div>
                            <div class="career__step_name">Begginer</div>
                        </div>
                        <div class="career__progress">
                            <div class="career__line">
                                <div class="career__line_legend">1<sup>st</sup> leg</div>
                                <div class="career__line_bar"><span style="width: 100%;"></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__line">
                                <div class="career__line_legend">2<sup>nd</sup> leg</div>
                                <div class="career__line_bar"><span style="width: 100%;"></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                        </div>
                        <div class="career__data">
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>BONUS</span>
                                        <strong>$ 200</strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PACKAGE</span>
                                        <strong>$100</strong>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>SALES</span>
                                        <strong></strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PERCENT</span>
                                        <strong>8.00%</strong>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="career career_active">
                        <div class="career__step">
                            <div class="career__step_number">
                                <strong>2</strong>
                                <span>LEVEL</span>
                            </div>
                            <div class="career__step_name">Junior <br/>agent</div>
                        </div>
                        <div class="career__progress">
                            <div class="career__line">
                                <div class="career__line_legend">1<sup>st</sup> leg</div>
                                <div class="career__line_bar"><span style="width: 10%;"></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__line">
                                <div class="career__line_legend">2<sup>nd</sup> leg</div>
                                <div class="career__line_bar"><span style="width: 10%;"></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                        </div>
                        <div class="career__data">
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>BONUS</span>
                                        <strong>$ 500</strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PACKAGE</span>
                                        <strong>$200</strong>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>SALES</span>
                                        <strong></strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PERCENT</span>
                                        <strong>10.00%</strong>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="career">
                        <div class="career__step">
                            <div class="career__step_number">
                                <strong>3</strong>
                                <span>LEVEL</span>
                            </div>
                            <div class="career__step_name">Agent</div>
                        </div>
                        <div class="career__progress">
                            <div class="career__line">
                                <div class="career__line_legend">1<sup>st</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__line">
                                <div class="career__line_legend">2<sup>nd</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__bonus"><span>Bonus or </span> <strong>IPhone</strong></div>
                        </div>
                        <div class="career__data">
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>BONUS</span>
                                        <strong>$ 1000</strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PACKAGE</span>
                                        <strong>$500</strong>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>SALES</span>
                                        <strong></strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PERCENT</span>
                                        <strong>11.00%</strong>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="career">
                        <div class="career__step">
                            <div class="career__step_number">
                                <strong>4</strong>
                                <span>LEVEL</span>
                            </div>
                            <div class="career__step_name">Senior <br/>agent</div>
                        </div>
                        <div class="career__progress">
                            <div class="career__line">
                                <div class="career__line_legend">1<sup>st</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__line">
                                <div class="career__line_legend">2<sup>nd</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__bonus"><span>Bonus or </span> <strong>Macbook Pro</strong></div>
                        </div>
                        <div class="career__data">
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>BONUS</span>
                                        <strong>$2000</strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PACKAGE</span>
                                        <strong>$1000</strong>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>SALES</span>
                                        <strong></strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PERCENT</span>
                                        <strong>12.00%</strong>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="career">
                        <div class="career__step">
                            <div class="career__step_number">
                                <strong>1</strong>
                                <span>LEVEL</span>
                            </div>
                            <div class="career__step_name">Junior <br/>manager</div>
                        </div>
                        <div class="career__progress">
                            <div class="career__line">
                                <div class="career__line_legend">1<sup>st</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                            <div class="career__line">
                                <div class="career__line_legend">2<sup>nd</sup> leg</div>
                                <div class="career__line_bar"><span></span></div>
                                <div class="career__line_progress">100%</div>
                            </div>
                        </div>
                        <div class="career__data">
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>BONUS</span>
                                        <strong>$5000</strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PACKAGE</span>
                                        <strong>$2000</strong>
                                    </div>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>SALES</span>
                                        <strong></strong>
                                    </div>
                                </li>
                                <li>
                                    <div class="career__data_icon">
                                        <img src="img/career__data_icon__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="career__data_value">
                                        <span>PERCENT</span>
                                        <strong>13.00%</strong>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
