<header class="header">
    <div class="container">

        <div class="header__row">
            <div class="header__left">
                <a class="header__logo" href="#">
                    <img src="img/header__logo.png" class="img-fluid">
                </a>
                <div class="header__time">2019-07-16   <span>14:14:48</span></div>
            </div>
            <div class="header__right">
                <div class="header__id"><span>ID:</span> <strong>zxf8302193</strong></div>
                <a class="header__auth" href="#"></a>
            </div>
            <a class="header__toggle nav_toggle" href="#">
                <span></span>
            </a>
        </div>

        <div class="header_nav">
            <div class="header_nav__user">
                <div class="header_nav__user_wrap">
                    <img src="img/nav__man.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="header_nav__wrap"></div>
            <div class="header_nav__inner">
                <a class="header_nav__icon icon_one nav_item nav_01" data-nav="01" href="#"><span></span></a>
                <a class="header_nav__icon icon_two nav_item nav_02" data-nav="02" href="#"><span></span></a>
                <a class="header_nav__icon icon_three nav_item nav_03" data-nav="03" href="#"><span></span></a>
                <a class="header_nav__icon icon_four nav_item nav_04" data-nav="04" href="#"><span></span></a>
                <div class="header_nav__hover"></div>
            </div>

            <a class="header_nav__text text_one nav_item nav_01" data-nav="01" href="#"><span>Tickets</span></a>
            <a class="header_nav__text text_two nav_item nav_02" data-nav="02" href="#"><span>Settings</span></a>
            <a class="header_nav__text text_three nav_item nav_03" data-nav="03" href="#"><span>Dashboard</span></a>
            <a class="header_nav__text text_four nav_item nav_04" data-nav="04" href="#"><span>Career</span></a>

        </div>

    </div>
</header>
