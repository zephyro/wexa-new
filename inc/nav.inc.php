<nav class="nav">
    <div class="nav__user">
        <a href="#" class="nav__user_id">VX83021933</a>
        <a class="nav__user_auth"></a>
    </div>
    <ul class="nav__mobile">
        <li><a href="#">Tickets</a></li>
        <li><a href="#">Settings</a></li>
        <li><a href="#">Dashboard</a></li>
        <li><a href="#">Career</a></li>
    </ul>
    <div class="nav__container">
        <ul class="nav__menu">
            <li class="active"><a href="#">Products</a></li>
            <li><a href="#">Marketing</a></li>
            <li>
                <a href="#">Partners</a>
                <ul class="nav__second">
                    <li><a href="#">Direct partners</a></li>
                    <li class="dropdown">
                        <a href="#">Marketing</a>
                        <ul>
                            <li><a href="#">Payouts</a></li>
                            <li><a href="#">Marketing</a></li>
                            <li><a href="#">Direct partners</a></li>
                            <li><a href="#">Buy packages</a></li>
                            <li><a href="#">Menu 4</a></li>
                            <li><a href="#">Menu 5</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Direct partners</a></li>
                    <li><a href="#">Buy packages</a></li>
                    <li><a href="#">Menu 4</a></li>
                    <li><a href="#">Menu 5</a></li>
                </ul>
            </li>
            <li><a href="#">Buy packages</a></li>
        </ul>
        <ul class="nav__menu">
            <li><a href="#">My packages</a></li>
            <li>
                <a href="#">History</a>
                <ul class="nav__second">
                    <li><a href="#">Payouts</a></li>
                    <li><a href="#">Marketing</a></li>
                    <li><a href="#">Direct partners</a></li>
                    <li><a href="#">Buy packages</a></li>
                    <li><a href="#">Menu 4</a></li>
                    <li><a href="#">Menu 5</a></li>
                </ul>
            </li>
            <li><a href="#">Refill</a></li>
            <li><a href="#">Money transfer</a></li>
        </ul>
    </div>
</nav>
<div class="nav__overlay nav_toggle"></div>

<div class="nav_animate">
    <div class="nav_animate__one"></div>
    <div class="nav_animate__two"></div>
    <div class="nav_animate__three"></div>
</div>