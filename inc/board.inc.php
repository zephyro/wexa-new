<div class="board">
    <div class="board__block board__block_one">
        <div class="board__level">
            <div class="board__level_value">
                <strong>5</strong>
                <span>level</span>
            </div>
            <div class="board__level_data">
                <table class="board__level_table">
                    <tr>
                        <td>Ref. percent:</td>
                        <td>6.00%</td>
                    </tr>
                    <tr>
                        <td>NEXT BONUS:</td>
                        <td>$ 200.00</td>
                    </tr>
                    <tr>
                        <td>NEED TURNOVER:</td>
                        <td>$ 10,000.50</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="board__block board__block_two">
        <ul class="board__status">
            <li>
                <div class="board__status_icon">
                    <img src="img/bottom__icon_01.png" class="img-fluid" alt="">
                </div>
                <div class="board__status_legend">
                    <span>BASIC</span>
                    <strong>320.00</strong>
                </div>
            </li>
            <li>
                <div class="board__status_icon">
                    <img src="img/bottom__icon_02.png" class="img-fluid" alt="">
                </div>
                <div class="board__status_legend">
                    <span>PREMIUM</span>
                    <strong>320.00</strong>
                </div>
            </li>
            <li>
                <div class="board__status_icon">
                    <img src="img/bottom__icon_03.png" class="img-fluid" alt="">
                </div>
                <div class="board__status_legend">
                    <span>EXCLUSIVE</span>
                    <strong>320.00</strong>
                </div>
            </li>
        </ul>
    </div>
    <div class="board__block board__block_three">
        <div class="board_info">
            <div class="board_info__item">
                <div class="board_info__icon">
                    <img src="img/bottom__icon_04.png" class="img-fluid" alt="">
                </div>
                <div class="board_info__data">
                    <span>MAIN BALANCE</span>
                    <strong>$ 12,295.50</strong>
                </div>
            </div>
            <div class="board_info__item">
                <div class="board_info__icon">
                    <img src="img/bottom__icon_05.png" class="img-fluid" alt="">
                </div>
                <div class="board_info__data">
                    <span>FUND BALANCE</span>
                    <strong>$500.00</strong>
                </div>
            </div>
            <div class="board_info__item">
                <div class="board_info__icon">
                    <img src="img/bottom__icon_06.png" class="img-fluid" alt="">
                </div>
                <div class="board_info__data">
                    <span>AFFILIATE BALANCE</span>
                    <strong>$ 11,000.50</strong>
                </div>
            </div>
            <div class="board_info__item">
                <div class="board_info__icon">
                    <img src="img/bottom__icon_07.png" class="img-fluid" alt="">
                </div>
                <div class="board_info__data">
                    <span>VEXA POINT</span>
                    <strong>350.00</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="board__block board__block_four">
        <div class="board__summary">
            <table class="board__summary_table">
                <tr>
                    <td>
                        <div class="board__summary_title">
                            <i><img src="img/board__icon_mini_01.png" class="img-fluid" alt=""></i>
                            <span>MY PACKAGES:</span>
                        </div>
                    </td>
                    <td>
                        <strong>$ 12,295.50</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="board__summary_title">
                            <i><img src="img/board__icon_mini_02.png" class="img-fluid" alt=""></i>
                            <span>AFFILIATE INCOME:</span>
                        </div>
                    </td>
                    <td>
                        <strong>$ 320.50</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="board__summary_title">
                            <i><img src="img/board__icon_mini_03.png" class="img-fluid" alt=""></i>
                            <span>TOTAL SALES:</span>
                        </div>
                    </td>
                    <td>
                        <strong>$ 200,300.50</strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>