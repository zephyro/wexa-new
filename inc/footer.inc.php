<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__left">
                <div class="footer__text">2019 Vexaglobal.com. All rights reserved.</div>
                <ul class="footer__nav">
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Support</a></li>
                </ul>
            </div>
            <div class="footer__right">
                <div class="lng">
                    <div class="lng__active">
                        <b><img src="img/lng__en.png" class="img-fluid" alt=""></b>
                        <span>english</span>
                    </div>
                    <ul class="lng__dropdown">
                        <li>
                            <a href="#">
                                <i><img src="img/lng__en.png" class="img-fluid" alt=""></i>
                                <span>english</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__ru.png" class="img-fluid" alt=""></i>
                                <span>русский</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__de.png" class="img-fluid" alt=""></i>
                                <span>deutsch</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i><img src="img/lng__pl.png" class="img-fluid" alt=""></i>
                                <span>poland</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>